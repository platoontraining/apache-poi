# Apache POI
### Setting up POI on local machine
* Grab the latest version of [Apache POI](https://mvnrepository.com/artifact/org.apache.poi/poi) and include it into you dependencies, it's easier than setting up the application via local  environment.


## Class Breakdown
### XSSFExpample Class
This class has two methods(aside from the Constructor which does nothing at this point)
* **creatingXSSFile()** - Creates an Excel file named "xssf_example.xlsx" in the root directory, in which the content has been pre-determined. Later on we will use this class to specify a class that we can load from the FE and the contents will be read dynamically


## Notes to consider
* [Apache POI Tutorial](https://www.journaldev.com/2562/apache-poi-tutorial)
* [Introduction to the Apache POI Library](https://dzone.com/articles/introduction-to-apache-poi-library)
* [Apache POI – Reading and Writing Excel file in Java](https://www.mkyong.com/java/apache-poi-reading-and-writing-excel-file-in-java/)
* [Apache POI Tutorial](https://www.tutorialspoint.com/apache_poi/index.htm)


# JAXB Binding
## Binding the data that we read from our Excel file to XML binding

JAXB is used to convert XML to Java objects and Java objects to XML. It's an API that gives us the power to read and write Java Objects to and from XML documents.


## Notes to consider
* [About JAXB (Java Architecture for XML Binding)](https://dzone.com/articles/jaxb-java-architecture-for-xml-binding)
* []()