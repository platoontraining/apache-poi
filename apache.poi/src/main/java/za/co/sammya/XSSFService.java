package za.co.sammya;

//import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.Font;

public class XSSFService {
	
	
	public XSSFService() {
		// TODO Auto-generated constructor stub
	}
	
	public void creatingXSSFile(String dirLocation, String fileName) throws Exception
	{
		int rownum;

        final String FILE_NAME = dirLocation + fileName;
		
		// Create Spreadsheet
		FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
		
		
		String[] names = {"Graiden Burgess", "Akeem Holloway", "Jerome Hicks", "Carson Hardy", "Stuart Medina", "Abraham Henderson", "Abbot Hernandez", "Sawyer Sparks", "Garrison Bell", "Castor Salazar", "Cain Farrell", "Herrod Gregory", "Brian Schroeder", "Cade Ward", "Blaze Johnson", "Alexander David", "Lyle Middleton", "Castor Jacobs", "Jonas Mitchell", "Lawrence Newman", "Ross Erickson", "Gary Ortega", "Quamar Murray", "Nicholas Chan", "Hedley Conway", "Kane Willis", "Zeus Cooke", "Richard Hood", "Jameson Mckinney", "Brendan Reid", "Kennan Hines", "Lev Salazar", "Julian Carter", "Silas Wright", "Stewart Valentine", "Avram Wilkins", "Knox Sosa", "Devin Nunez", "Linus Fowler", "Alfonso Skinner", "Gage Duffy", "Jason Mcmillan", "Flynn Jenkins", "Acton Bowman", "Brian Terrell", "Bevis Wooten", "Ferdinand Ortiz", "Gray Walter", "Quamar Patton", "Aristotle Duncan"};
		String[] city = {"Leerbeek", "Verona", "Barghe", "Kitzbühel", "Ramsey", "Bognor Regis", "Río Ibáñez", "Vilvoorde", "Holman", "Fermont", "Cambridge", "Charleroi", "Crato", "Neyveli", "Romford", "Norfolk", "Cholet", "Hofors", "Fort William", "Rutigliano", "Ulm", "Colledimacine", "Guna", "Saint-Pierre", "Monceau-Imbrechies", "Bracknell", "Wechelderzande", "Canning", "Bansberia", "GŽrouville", "Colina", "Haguenau", "Port Hope", "Atlanta", "Salihli", "Kerkhove", "Cholchol", "Moio Alcantara", "Coldstream", "Gore", 
		"Tucson", "Keumiee", "San Pancrazio Salentino", "Puyehue", "Manchester", "Jemeppe-sur-Sambre", "Banbury", "North Las Vegas", "Orroli", "Sint-Martens-Lennik"};
		
		
		Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        sheet.setColumnWidth((short) 0, (short)((50 * 8) / ((double) 1 / 20)));
        sheet.setColumnWidth((short) 1, (short)((50 * 8) / ((double) 1 / 20)));
        workbook.setSheetName(0, "SA Music Podcasts");
        
        Font font1 = workbook.createFont();
        font1.setFontHeightInPoints((short) 10);
        font1.setColor((short) 0xc); // make it blue
        font1.setBold(true);
        XSSFCellStyle cellStyle1 = (XSSFCellStyle) workbook.createCellStyle();
        cellStyle1.setFont(font1);
        
        Font font2 = workbook.createFont();
        font2.setFontHeightInPoints((short) 10);
        font2.setColor((short) Font.COLOR_NORMAL);
        XSSFCellStyle cellStyle2 = (XSSFCellStyle) workbook.createCellStyle();
        cellStyle2.setFont(font2);
        
        // Label the Spreadsheet cells accordingly
        Row headerRow = sheet.createRow(0);
        Cell cell1 = headerRow.createCell(0);
        cell1.setCellValue("Name");
        cell1.setCellStyle(cellStyle1);

        Cell cell2 = headerRow.createCell(1);
        cell2.setCellValue("City");
        cell2.setCellStyle(cellStyle1);
        
        Cell cell3 = headerRow.createCell(2);
        cell3.setCellValue("Entry Date");
        cell3.setCellStyle(cellStyle2);
		
        
        Row row = null;
        Cell cell = null;
        for (rownum = (short) 1; rownum <= names.length; rownum++) {
            row = sheet.createRow(rownum);
            cell = row.createCell(0);
            cell.setCellValue(names[rownum - 1]);
            cell.setCellStyle(cellStyle2);
            
            cell = row.createCell(1);
            cell.setCellValue(city[rownum - 1]);
            cell.setCellStyle(cellStyle2);
            
            cell = row.createCell(2);
            cell.setCellValue((java.time.LocalDate.now()).toString());
            cell.setCellStyle(cellStyle2);
        }
		
        
        // Write to file
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
        System.out.println("Number Of Names: " + names.length + " entries.\n");
		System.out.println("Creating File: " + FILE_NAME + ".\nFile creation complete.\n");
		
	}
	
	public void readingXSSFile(String dirLocation, String fileName) throws Exception
	{
		// File to open
		final String FILE_NAME = dirLocation + fileName;
		
        FileInputStream excelInputStream = new FileInputStream(new File(FILE_NAME));
        Workbook workbook = new XSSFWorkbook(excelInputStream);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowItr = sheet.iterator();
        int rowNum = 0;
        
        while (rowItr.hasNext()) {
            Row row = rowItr.next();
            Iterator<Cell> cellItr = row.iterator();
            //System.out.print(rowNum + ". ");
            while (cellItr.hasNext()) {
                Cell cell = cellItr.next();
                
                // Write entry to XML Here
                // Static XML Data
//                PodcastIndexService podcast_1 = new PodcastIndexService();
//                podcast_1.setArtistName("Sammy");
//                podcast_1.setPodcastName("Building the web with Java");
//                podcast_1.setPublishDate((java.time.LocalDate.now()).toString());
//                podcastList.add(podcast_1);
//                
//                PodcastIndexService podcast_2 = new PodcastIndexService();
//                podcast_2.setArtistName("Greg");
//                podcast_2.setPodcastName("Some great news about Hippies");
//                podcast_2.setPublishDate((java.time.LocalDate.now()).toString());
//                podcastList.add(podcast_2);
                
                /*
                 * The data needs to be written in to an XML file dynamically
                 * Use this location to complete that task
                 * */
                
//                if (cell.getCellTypeEnum() == CellType.STRING) {
//                    System.out.print(cell.getStringCellValue() + "\t\t");
//                } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
//                    System.out.print(cell.getNumericCellValue() + "\t\t");
//                }
            }
            //System.out.println();
            rowNum++;
        }
        workbook.close();
        excelInputStream.close();
        System.out.println("Number of rows read: " + (rowNum - 1) + ".");
		System.out.println("End Reading File.\n");
		
    }
	
	public int numOfRecords(String dirLocation, String fileName) throws Exception
	{
		int rowNum = 0;
		
		final String FILE_NAME = dirLocation + fileName;
		
        FileInputStream excelInputStream = new FileInputStream(new File(FILE_NAME));
        Workbook workbook = new XSSFWorkbook(excelInputStream);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowItr = sheet.iterator();
                
        while (rowItr.hasNext()) {
            Row row = rowItr.next();
            Iterator<Cell> cellItr = row.iterator();
            while (cellItr.hasNext()) {
                Cell cell = cellItr.next();
            }
            rowNum++;
        }
        workbook.close();
        excelInputStream.close();
		
		return rowNum; // Return the number of rows that appear in the file we specified
		
	}
}
