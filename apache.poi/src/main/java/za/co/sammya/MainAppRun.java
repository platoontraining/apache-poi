package za.co.sammya;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class MainAppRun 
{
	private static final String PODCAST_XML = "./files/global-register-converted-" + (java.time.LocalDate.now()).toString() + ".xml";
	
    public static void main( String[] args ) throws Exception, JAXBException, IOException
    {
        String folderLocation = "./files/";
        String fileName = "";
        String theTime = "";

        // if directory does note exist then create it
   		File directory = new File(folderLocation);
   		if(!directory.exists())
   		{
   			directory.mkdir();
   		}
   		// ===============END=========================
        
        // Objects
        XSSFService xssfServiceCalls = new XSSFService();
        
        theTime = (java.time.LocalDate.now()).toString();
        fileName = "global-register-" + theTime + ".xlsx"; // Spreadsheet that will be our working file
        
        try {
	        xssfServiceCalls.creatingXSSFile(folderLocation, fileName);
	        
        }catch(Exception e)
        {
        	System.out.println("Exception occurred in creating the file. Error: " + e + " raised.\n");
        }

        try {
        	xssfServiceCalls.readingXSSFile(folderLocation, fileName);
        }catch(Exception re)
        {
        	System.out.println("Exception occurred in reading the file. Error: " + re + " raised.\n");
        }
        
        
        // Working with JAXB & XML Files
        ArrayList<PodcastIndexService> podcastList = new ArrayList<PodcastIndexService>();
        
        System.out.println("Number of records: " + ((xssfServiceCalls.numOfRecords(folderLocation, fileName))-1));
        System.out.println("=====================================================================================");
        System.out.println("*************************************************************************************");
        System.out.println("#####################################################################################");
        
        
        // Static XML Data
        PodcastIndexService podcast_1 = new PodcastIndexService();
        podcast_1.setArtistName("Sammy");
        podcast_1.setPodcastName("Building the web with Java");
        podcast_1.setPublishDate((java.time.LocalDate.now()).toString());
        podcastList.add(podcast_1);
        
        PodcastIndexService podcast_2 = new PodcastIndexService();
        podcast_2.setArtistName("Greg");
        podcast_2.setPodcastName("Some great news about Hippies");
        podcast_2.setPublishDate((java.time.LocalDate.now()).toString());
        podcastList.add(podcast_2);
        
        
        PodcastIndexServiceImpl podcastImpl = new PodcastIndexServiceImpl();
        podcastImpl.setArtistName("DJ Whoever");
        podcastImpl.setPodcastName("Somethine other than coding, all music");
        podcastImpl.setPodcastList(podcastList);
        
        javaObjectToXML(podcastImpl);
        XMLToObject();
        
        
        
        System.out.println("=====================================================================================");
        
        
        // NOTE THAT THIS IS THE FINAL STEP IN THE APPLICATION
        /*
         * Excel Spreedsheet to XML
         * 
         *   1. Read the spreadsheet
         *   2. Convert entries into objects
         *   	2.1 Remember that the first row contains column headings
         *   3. Add objects to XML
         *   4. Write XML File
         */
    }
    
    /**
     * Java Object to XML
     * @param cdStore
     * @throws JAXBException
     */
    public static void javaObjectToXML(PodcastIndexServiceImpl podcastServ) throws JAXBException
    {
    	
    	// Create JAXB Context And Instance Masrsheller
    	JAXBContext context = JAXBContext.newInstance(PodcastIndexServiceImpl.class);
    	Marshaller mars = context.createMarshaller();
    	mars.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    	
    	// Write to System out
    	// mars.marshal(podcastServ, System.out);
    	
    	// Write to file
    	mars.marshal(podcastServ, new File(PODCAST_XML));
    }
    
    /**
     * XML to java Object
     * @throws JAXBException
     */
    private static void XMLToObject() throws JAXBException
    {
    	
    	// Create JAXB Context and instatiate UnMarshaler
    	JAXBContext context = JAXBContext.newInstance(PodcastIndexServiceImpl.class);
    	Unmarshaller uMars = context.createUnmarshaller();
    	Object obj = uMars.unmarshal(new File(PODCAST_XML));
    	
    	if(obj instanceof PodcastIndexServiceImpl)
    	{
    		PodcastIndexServiceImpl pIS = (PodcastIndexServiceImpl) obj;
    		System.out.println("=============== PRINTING XML TO JAVA LIBRARY==============");
    		System.out.println("Artist Name" + pIS.getArtistName());
    		System.out.println("Podcast Name" + pIS.getPodcastName());
    		
    		for(PodcastIndexService podIndexServ : pIS.getPodcastList())
    		{
    			System.out.println(podIndexServ.getArtistName());
    			System.out.println(podIndexServ.getPodcastName());
    			System.out.println("==========================================================");
    		}
    		
    	}
    }
}

