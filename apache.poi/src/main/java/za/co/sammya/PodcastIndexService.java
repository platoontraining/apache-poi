package za.co.sammya;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

// CD
@XmlRootElement(name = "podcast")
// Defining the order in which the fields are written
@XmlType(propOrder = {"artistName", "podcastName", "publishDate"})
public class PodcastIndexService {
	
	private String artistName;
    private String podcastName;
    private String publishDate;
	
    
    @XmlElement(name = "artistName") 
    public String getArtistName() {
		return artistName;
	}
    
    
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getPodcastName() {
		return podcastName;
	}
	public void setPodcastName(String podcastName) {
		this.podcastName = podcastName;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

    
}
