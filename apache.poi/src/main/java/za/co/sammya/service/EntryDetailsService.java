package za.co.sammya.service;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "details")
@XmlType(propOrder = {"personalName", "locationCity", "entryDate"})
public class EntryDetailsService {

	private String personalName;
	private String locationCity;
	private String entryDate;
	
	@XmlElement(name = "personalName")
	public String getPersonalName()
	{
		return personalName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	
	
}
