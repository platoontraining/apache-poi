package za.co.sammya;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


//This statement means that class "PodcastIndexServiceImpl.java" is the root-element of our example
@XmlRootElement(namespace = "za.co.sammysa")
public class PodcastIndexServiceImpl {

	//CDStore
	
	//XMLElementWrapper generates a wrapper element around XML representation
	@XmlElementWrapper(name = "podcastList")
	
	//XMLElement sets the name of the entities
	@XmlElement(name = "podcast")
	private ArrayList<PodcastIndexService> thisPodcastList;
	private String artistName;
	private String podcastName;
	
	
	public void setPodcastList(ArrayList<PodcastIndexService> thisPodcastList)
	{
		this.thisPodcastList = thisPodcastList;
	}
	public ArrayList<PodcastIndexService> getPodcastList()
	{
		return thisPodcastList;
	}
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public String getPodcastName() {
		return podcastName;
	}
	public void setPodcastName(String podcastName) {
		this.podcastName = podcastName;
	}
	
}
